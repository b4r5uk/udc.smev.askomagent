﻿using System;
using Dapper;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;

namespace udc.smev.askomagent
{
    internal class AskomServerSqlDb
    {
        private string _serverAddress;
        private string _username;
        private string _password;
        private string sqlConnString;

        public AskomServerSqlDb(string serverAddress, string sqlServerUsername, string sqlServerPassword)
        {
            _serverAddress = serverAddress;
            this._username = sqlServerUsername;
            this._password = sqlServerPassword;

            this.sqlConnString = "Data Source = " + _serverAddress + "; User ID = " + _username + "; Password=" + _password + "; Initial Catalog = SMEVRequests";            
        }

        internal List<string> GetMvdFailedQueries_DONTWORK_TODELETE(DateTime fromDate)
        {
            string sql = AskomServerSqlQueries.GetMvdFailedQueries;
            using (var connection = new SqlConnection(sqlConnString))
            {
                string dateSqlFormat = fromDate.ToString("yyyy-MM-dd");
                var failedQueriesPassports = connection.Query<string>(sql, new { fromDate = dateSqlFormat }).ToList();
                return failedQueriesPassports;                
            }
        }

        internal List<string> GetMvdQueryIdsAfterDate(DateTime fromDate)
        {
            string sql = AskomServerSqlQueries.GetMvdQueryIdsAfterDate;
            using (var connection = new SqlConnection(sqlConnString))
            {
                string dateSqlFormat = fromDate.ToString("yyyy-MM-dd");
                var ids = connection.Query<string>(sql, new { fromDate = dateSqlFormat }).ToList();
                return ids;
            }
        }
    }
}