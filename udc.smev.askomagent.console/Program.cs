﻿using System;
using System.Linq;

namespace udc.smev.askomagent.console
{
    class Program
    {        
        static void Main(string[] args)
        {
            Console.WriteLine("args: " + args.Length.ToString());
            Console.WriteLine(args[0]);
            Console.WriteLine(args[1]);

            if (args.Contains("-mvdresendfailed"))
            {
                Console.WriteLine("start Resend Function");
                var _askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");
                var _lastResentQuery = DateTime.MinValue;
                var _sender = new SenderInfo(firstName: "Станислав",
                    lastName: "Тадтаев",
                    middleName: "Владимирович",
                    mvdOrganizationId: "O0000000320");
                _askomServer.BadPassportOccured += _askomServer_BadPassportOccured;
                _askomServer.ResendMvdFailedQueries(fromDate: DateTime.Now.AddDays(-10), _sender, "mail.comita.spb.ru");

            }
            else
            {
                
                Console.WriteLine("Usage");
                Console.WriteLine("-mvdresendfailed - Resend Failed Requests");
            }
        }

        private static void _askomServer_BadPassportOccured(SmevMvdQuery smevMvdQuery)
        {
            Console.WriteLine("пасспорт с проблемами:");
            Console.WriteLine(smevMvdQuery._mvdShortRequestInfo.series +" : "+ smevMvdQuery._mvdShortRequestInfo.number);
        }
    }

    
}
