using System;
using System.Linq;
using System.Threading;
using Xunit;
using Xunit.Abstractions;

namespace udc.smev.askomagent.tests
{
    public class SmevQueryTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public SmevQueryTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        string egrulGood = "7839076451";
        string askomServerAddressGood = "udcsmevold.comita.lan";

        [Fact]
        public void Create_EgrulSimplest_Ok()
        {
            EgrulSmevQuery q = new EgrulSmevQuery("7839076451");
            Assert.NotNull(q);
        }

        [Fact]
        public void Create_MvdQuery_Ok()
        {
            var pi = new PassportInfo(
                passportSeries: "4005",
                passportNumber: "589042"
                );

            var si = new SenderInfo(
                firstName: "Станислав",
                lastName: "Тадтаев",
                middleName: "Владимирович",
                mvdOrganizationId: "O0000000320"
                );
            var askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");

            var mvdQueryPassportShort = new SmevMvdQuery(passportInfo: pi, senderInfo: si, askomServer);
            var askomRequestId = mvdQueryPassportShort.Send();
            _testOutputHelper.WriteLine(askomRequestId.ToString());
            Assert.IsType<string>(askomRequestId);

        }

        [Fact]
        public void ResendFailedWithServerHelperMethod_ok()
        {
            var si = new SenderInfo(
                firstName: "Станислав",
                lastName: "Тадтаев",
                middleName: "Владимирович",
                mvdOrganizationId: "O0000000320"
                );

            var askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");
            askomServer.ResendMvdFailedQueries(DateTime.Now.AddDays(-2), si, "mail");
        }

        [Fact]
        public void Get_IsMvdQuerySent_Ok()
        {
            var askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");

            var mvdQueryPassportShort = new SmevMvdQuery(askomRequestId: "90133", askomServer);
            Assert.True(mvdQueryPassportShort.IsResultReceived);

        }
        [Fact]
        public void Get_IsMvdQuerySent_Failed_DvoinoiKodGuError()
        {
            var askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");

            var mvdQueryPassportShort = new SmevMvdQuery(askomRequestId: "90226", askomServer);
            Assert.False(mvdQueryPassportShort.IsResultReceived);

        }
    }
}
