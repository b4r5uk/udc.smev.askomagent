SELECT distinct
	--r.Id 
	--r.Date, 
	--ops.InternalName, 
	--r.ServiceOperationId, 	
	--r.State, 
	--ri.ParentId, 
	--ri.Error,
	r.RequestContentsAnnotation PassportSeriesNumber
FROM
	Requests r
		INNER JOIN RequestItems ri
	ON r.Id = ri.RequestId 
		INNER JOIN Operations ops
	ON r.ServiceOperationId = ops.Id
where 
	r.ServiceOperationId = 8
	AND
	r.State = 0
	and
	ri.ParentId is not null
	and
	r.RequestContentsAnnotation != ''
	-- date format yyyy-mm-dd
	and
	r.Date >= @fromDate 