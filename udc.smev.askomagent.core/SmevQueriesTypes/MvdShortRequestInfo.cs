﻿namespace udc.smev.askomagent
{
    public class MvdShortRequestInfo
    {
        public readonly string series;
        public readonly string number;
        public readonly string firstName;
        public readonly string lastName;
        public readonly string middleName;
        public readonly string organisationId;

        public MvdShortRequestInfo(PassportInfo passportInfo, SenderInfo senderInfo)
        {
            this.series = passportInfo.PassportSeries;
            this.number = passportInfo.PassportNumber;
            this.firstName = senderInfo.FirstName;
            this.lastName = senderInfo.LastName;
            this.middleName = senderInfo.MiddleName;
            this.organisationId = senderInfo.MvdOrganizationId;
        }
        
    }
}