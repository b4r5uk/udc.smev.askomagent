﻿namespace udc.smev.askomagent
{
    public class SenderInfo
    {
        public readonly string FirstName;
        public readonly string LastName;
        public readonly string MiddleName;
        public readonly string MvdOrganizationId;

        public SenderInfo(string firstName, string lastName, string middleName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MiddleName = middleName;
        }
        public SenderInfo(string firstName, string lastName, string middleName, string mvdOrganizationId) : this(firstName, lastName, middleName)
        {
            this.MvdOrganizationId = mvdOrganizationId;
        }        
    }
}