﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace udc.smev.askomagent
{
    internal class AskomServerRestApi
    {
        private readonly ITestOutputHelper _testOutputHelper;

        //
        private static readonly HttpClient client = new HttpClient();
        private string egrulUrl = "/api/FNS/Egrul/Excerpt/send";
        private string mvdUrl = "/api/Mvd/Skmvd/CheckValidPassport/send";
        private string sentQueriesUrl = "/api/requests/";


        private string _serverAddress;
        private string _apiKey;

        public AskomServerRestApi(string serverAddress, string apiKey)
        {
            this._serverAddress = serverAddress;
            this._apiKey = apiKey;

        }

        internal async void SendEgrulSimple(string inn)
        {
            string egrulSendUrl = "http://" + _serverAddress + egrulUrl;

            string myJson = "{'item': 'inn','type':'false'}";
            using (var client = new HttpClient())
            {
                var response = await client.PostAsync(egrulSendUrl, new StringContent(myJson, Encoding.UTF8, "application/json"));
            }

        }

        internal async Task<string> SendMvdShort(string mvdShortReqJson)
        {
            string mvdSendUrl = "http://" + _serverAddress + mvdUrl;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);
                var response = await client.PostAsync(mvdSendUrl, new StringContent(mvdShortReqJson, Encoding.UTF8, "application/json"));
                var content = response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<AskomSentQuerySyncResponse>(content.Result);
                
                return result.requestId;
            }

        }

        internal string GetMvdPassportInfoFromQueriId(string queryId)
        {
            string _passportInfo = null;
            string askomSentQueriesurl = "http://" + _serverAddress + sentQueriesUrl + queryId + "/result";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);
                var response = client.GetAsync(askomSentQueriesurl, HttpCompletionOption.ResponseContentRead).Result;
                var contentStr = response.Content.ReadAsStringAsync();
                var contentObj = JsonConvert.DeserializeObject<AskomSentQueryState>(contentStr.Result);
                if (contentObj.response.body != null)
                {
                    _passportInfo = contentObj.response.body.series + " " + contentObj.response.body.Number;
                }
                                
            }
            return _passportInfo;
        }

        internal async Task<bool> IsPassportOk(string queryId)
        {
            bool _passportOk = false;
            string askomSentQueriesurl = "http://" + _serverAddress + sentQueriesUrl + queryId + "/result";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);
                var response = await client.GetAsync(askomSentQueriesurl, HttpCompletionOption.ResponseContentRead);
                var contentStr = response.Content.ReadAsStringAsync();
                var contentObj = JsonConvert.DeserializeObject<AskomSentQueryState>(contentStr.Result);
                var state = contentObj.response.body.State;


                if (state == "Действителен")
                {
                    _passportOk = true;
                }
                return _passportOk;
            }
        }

        internal async Task<bool> IsResultReceived(string id)
        {
            bool _isRequestSent = false;
            string askomSentQueriesurl = "http://" + _serverAddress + sentQueriesUrl + id + "/result";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);
                var response = await client.GetAsync(askomSentQueriesurl, HttpCompletionOption.ResponseContentRead);
                var contentStr = response.Content.ReadAsStringAsync();
                var contentObj = JsonConvert.DeserializeObject<AskomSentQueryState>(contentStr.Result);
                var status = contentObj.response.Status;
                
                if (status == "RESULT")
                {
                    _isRequestSent = true;
                }
                return _isRequestSent;
            }
        }
    }
}