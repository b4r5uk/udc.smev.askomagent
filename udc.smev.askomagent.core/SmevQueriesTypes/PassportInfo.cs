﻿using System;
using System.Collections.Generic;
using System.Text;

namespace udc.smev.askomagent
{
    public class PassportInfo
    {
        public PassportInfo(string passportSeries, string passportNumber)
        {
            this.PassportSeries = passportSeries;
            this.PassportNumber = passportNumber;
        }
        public readonly string PassportSeries;
        public readonly string PassportNumber;
    }
}
