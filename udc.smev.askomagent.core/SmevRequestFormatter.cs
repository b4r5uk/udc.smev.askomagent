﻿using System;
using Newtonsoft.Json;

namespace udc.smev.askomagent
{
    internal static class SmevRequestFormatter
    {
        internal static string MvdShort(MvdShortRequestInfo mvdShortReqInfo)
        {
            string mvdReqJson = JsonConvert.SerializeObject(mvdShortReqInfo);
            return mvdReqJson;
        }
    }
}