FROM mcr.microsoft.com/dotnet/core/runtime:2.1-stretch-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["udc.smev.askomagent.console/udc.smev.askomagent.console.csproj", "udc.smev.askomagent.console/"]
COPY ["udc.smev.askomagent.core/udc.smev.askomagent.core.csproj", "udc.smev.askomagent.core/"]
RUN dotnet restore "udc.smev.askomagent.console/udc.smev.askomagent.console.csproj"
COPY . .
WORKDIR "/src/udc.smev.askomagent.console"
RUN dotnet build "udc.smev.askomagent.console.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "udc.smev.askomagent.console.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "udc.smev.askomagent.console.dll", "-mvdresendfailed"]