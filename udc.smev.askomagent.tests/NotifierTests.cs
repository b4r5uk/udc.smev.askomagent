﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace udc.smev.askomagent.tests
{
    public class NotifierTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly AskomServer _askomServer;

        public NotifierTests(ITestOutputHelper testOutputHelper)
        {
            this._testOutputHelper = testOutputHelper;
            this._askomServer = new AskomServer(serverAddress: "udcsmevold.comita.lan", apiKey: "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd");
        }

        [Fact]
        public void BadMvdQuery_operatorsTsv_ok()
        {
            Notifier.BadMvdQuery(new SmevMvdQuery("226466", _askomServer));
                

        }
    }
}
