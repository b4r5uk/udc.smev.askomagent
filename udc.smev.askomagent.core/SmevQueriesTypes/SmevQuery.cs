﻿using System;
using System.Collections.Generic;
using System.Text;

namespace udc.smev.askomagent
{
    public interface ISmevQuery
    {
        string Id { get; }
        int Send(AskomServer askomServer);
    }
}
