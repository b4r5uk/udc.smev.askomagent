﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace udc.smev.askomagent
{
    public class AskomServer
    {
        public delegate void MvdBadPassportHandler(SmevMvdQuery smevMvdQuery);
        public event MvdBadPassportHandler BadPassportOccured;

        private DateTime defaultDate = DateTime.MinValue;
        private readonly string _serverAddress;
        private readonly string _apiKey;
        
        public AskomServer(string serverAddress, string apiKey)
        {
            this._serverAddress = serverAddress;
            this._apiKey = apiKey;
        }

        public List<ISmevQuery> GetAllQueries()
        {
            var askomSql = new AskomServerSqlDb(_serverAddress, "sa", "123qweASD");
            throw new NotImplementedException();
            //return new List<ISmevQuery>();
        }

        public List<string> GetMvdQueryIdsAfterDate(DateTime fromDate)
        {
            var askomSql = new AskomServerSqlDb(_serverAddress, "sa", "123qweASD");
            return askomSql.GetMvdQueryIdsAfterDate(fromDate: fromDate);
        }
        /// <summary>
        /// problem: bad smell when take passport info from second api request after check was problem...
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        public List<string> GetFailedMvdQueriesPassports(DateTime fromDate)
        {
            var failedPassports = new List<string>();

            var mvdQueriesIdTocheck = this.GetMvdQueryIdsAfterDate(fromDate);
            Console.WriteLine("queries from sql to check: " + mvdQueriesIdTocheck.Count);
            foreach (var queryId in mvdQueriesIdTocheck)
            {
                Console.WriteLine("checking " + queryId);
                if (!this.IsResultReceived(queryId))
                {
                    Console.WriteLine("bad ID " + queryId + " !");
                    string pinfo = this.GetMvdPassportInfoFromQueriId(queryId);
                    if (pinfo != null)
                    {
                        failedPassports.Add(pinfo);
                    }                    
                }
            }
            return failedPassports;
        }

        /// <summary>
        /// problem: req id  91035 - cant get passport info from api - empty :((((
        /// </summary>
        /// <param name="queryId"></param>
        /// <returns></returns>
        private string GetMvdPassportInfoFromQueriId(string queryId)
        {
            var askomRest = new AskomServerRestApi(this._serverAddress, this._apiKey);
            var response = askomRest.GetMvdPassportInfoFromQueriId(queryId);
            return response;

        }

        public List<string> GetFailedMvdQueriesPassports()
        {
            return GetFailedMvdQueriesPassports(DateTime.MinValue);
        }

        public void SendQueryEgrulShort(EgrulSmevQuery q)
        {
            var askomServerRestApi = new AskomServerRestApi(_serverAddress, _apiKey);
            askomServerRestApi.SendEgrulSimple(q.Inn);

        }

        internal string SendMvdShort(MvdShortRequestInfo mvdShortRequestInfo)
        {
            var askomServerRestApi = new AskomServerRestApi(_serverAddress, _apiKey);
            string mvdShortReqJson = SmevRequestFormatter.MvdShort(mvdShortRequestInfo);
            return askomServerRestApi.SendMvdShort(mvdShortReqJson).Result;

        }

     

        public bool IsPassportOk(string queryId)
        {
            var askomServerRestApi = new AskomServerRestApi(_serverAddress, _apiKey);
            var response = askomServerRestApi.IsPassportOk(queryId);
            return response.Result;
        }

        internal bool IsResultReceived(string id)
        {
            var askomServerRestApi = new AskomServerRestApi(_serverAddress, _apiKey);
            var result = askomServerRestApi.IsResultReceived(id);
            return result.Result;
        }

        public void ResendMvdFailedQueries(DateTime fromDate, SenderInfo sender, string smtpServer, int maxReqsPerMin = 30)
        {
            //var _notifier = new Notifier(smtpServer);
            
            var mvdFailedQueriesPassportsInfo = this.GetFailedMvdQueriesPassports(fromDate: fromDate);
            Console.WriteLine("bad count: " + mvdFailedQueriesPassportsInfo.Count);

            foreach (string passport in mvdFailedQueriesPassportsInfo)
            {
                var pi = new PassportInfo(
                passportSeries: passport.Trim().Split(' ')[0],
                passportNumber: passport.Trim().Split(' ')[1]
                );
                var newMvdShortQuery = new SmevMvdQuery(passportInfo: pi, senderInfo: sender, this);
                newMvdShortQuery.Send();

                if (newMvdShortQuery.IsResultReceived && !newMvdShortQuery.IsPassportOk)
                {
                    var p = 12;
                    if (BadPassportOccured != null)
                    {
                        BadPassportOccured(newMvdShortQuery);
                    }                    
                }

                Thread.Sleep(2500);
            }


        }
    }
}