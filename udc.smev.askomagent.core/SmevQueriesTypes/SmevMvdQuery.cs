﻿using System;
using System.Collections.Generic;
using System.Text;

namespace udc.smev.askomagent
{
    public class SmevMvdQuery
    {
        public readonly MvdShortRequestInfo _mvdShortRequestInfo;
        private readonly AskomServer _askomServer;
        public DateTime RequestDateTime;
        public DateTime ResponseDateTime;
        
        public string Id { get; private set; }

        public SmevMvdQuery(PassportInfo passportInfo, SenderInfo senderInfo, AskomServer askomServer)
        {
            this._mvdShortRequestInfo = new MvdShortRequestInfo(passportInfo, senderInfo);
            this._askomServer = askomServer;
        }

        public SmevMvdQuery(string askomRequestId, AskomServer askomServer)
        {
            this.Id = askomRequestId;
            this._askomServer = askomServer;
        }

        public string Send()
        {
            var _id = _askomServer.SendMvdShort(_mvdShortRequestInfo);
            this.Id = _id;
            return _id;
        }

        public bool IsResultReceived
        {
            get
            {
                bool _value = false;
                try
                {
                    _value = _askomServer.IsResultReceived(this.Id);
                }
                catch
                {
                    throw;
                }
                return _value;
            }

        }

        public bool IsPassportOk
        {
            get
            {
                bool _value = false;
                try
                {
                    _value = _askomServer.IsPassportOk(this.Id);
                }
                catch
                {
                    throw;
                }
                return _value;
            }
        }

    }
}
