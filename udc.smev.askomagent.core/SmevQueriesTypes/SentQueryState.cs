﻿namespace udc.smev.askomagent
{
    public class AskomSentQueryState
    {
        public string message { get; set; }
        public bool isResponseReceived { get; set; }
        public Response response { get; set; }
    }

    public class Response
    {
        public string Id { get; set; }
        public Body body { get; set; }
        public string Status { get; set; }
        //public string Signature { get; set; }
        //public string Fault { get; set; }
        //public string AppDocument { get; set; }
    }

    public class Body
    {
        public string Number { get; set; }
        public string series { get; set; }
        public string State { get; set; }
        public string Date { get; set; }
        public string Reason { get; set; }
        public string Okato { get; set; }
    }
}
