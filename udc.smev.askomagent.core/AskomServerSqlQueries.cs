﻿namespace udc.smev.askomagent
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    internal static class AskomServerSqlQueries
    {        
        private static string ReadResourceSql(string filename)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith(filename));

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static string GetMvdFailedQueries
        {
            get
            {
                return ReadResourceSql("GetMvdFailedQueries.sql");
            }
        }

        public static string GetMvdQueryIdsAfterDate
        {
            get
            {
                return ReadResourceSql("GetMvdQueryIdsAfterDate.sql");
            }
        }
    }
}