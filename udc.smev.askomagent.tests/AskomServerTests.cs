﻿using Xunit;
using Xunit.Abstractions;

namespace udc.smev.askomagent.tests
{
    public class AskomServerTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public AskomServerTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }
        string egrulGood = "7839076451";
        string askomServerAddressGood = "udcsmevold.comita.lan";
        string apiKey = "mujq5jka3zswcpek38o9ecm3stoaow5e7j724x5hz4u0p290tj7xpr8kip8m5jwh4nz2rmqfd";

        [Theory]
        [InlineData("udcsmev.comita.lan")]
        public void AskomServer_Created_ok(string address)
        {
            var askom = new AskomServer(address, apiKey);
            Assert.NotNull(askom);
        }

        [Fact]
        public void Send_Egrul_Ok()
        {
            EgrulSmevQuery q = new EgrulSmevQuery(egrulGood);
            var askom = new AskomServer(askomServerAddressGood, apiKey);
            askom.SendQueryEgrulShort(q);
        }

        [Fact]
        public void Get_FailedMvdQueriesPassportInfoString_ok()
        {
            var askom = new AskomServer(askomServerAddressGood, apiKey);
            var pinfo = askom.GetFailedMvdQueriesPassports();
            _testOutputHelper.WriteLine(string.Join(',', pinfo.ToArray()));
        }

        [Fact]
        public void Get_MvdQueryResult_Deistvitelen()
        {
            var askom = new AskomServer(askomServerAddressGood, apiKey);
            //string queryId = "90990";
            string queryId = " 91087";
            Assert.True(askom.IsPassportOk(queryId));
        }
    }
}
